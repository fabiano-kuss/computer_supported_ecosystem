#!/bin/bash

#Aulacast Certificate Generate
#Copyright (C) 2019  Fabiano Sardenberg Kuss

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ROOTDIR=$(head root_dir.cfg)
CN=$1
EMAIL=$2
INTERMEDIATE=$3
FILENAME=$(echo $CN  | sed 's/ /_/g')

#replaced_dir=$(echo $ROOTDIR | sed 's/\//\\\//g')

#cp openssl.user.cnf $ROOTDIR/$INTERMEDIATE
cp $INTERMEDIATE/openssl.intermediate.cnf tmp.openssl.user.cnf

#sed -i "s/-ROOTDIR-/$replaced_dir/g" $ROOTDIR/openssl.user.cnf
#sed -i "s/-ROOTDIR-/$replaced_dir/g" $ROOTDIR/$INTERMEDIATE/openssl.user.cnf
#sed -i "s/-intermediate-/$INTERMEDIATE/g" $ROOTDIR/$INTERMEDIATE/openssl.user.cnf

#cd $ROOTDIR/$INTERMEDIATE


#touch index.txt
#[ ! -f serial ] && echo 1000 > serial
#echo unique_subject = yes > index.txt.attr

if [ -z "$4" ]
then
	echo "Using ECC key"
	openssl ecparam -genkey -name secp384r1 -out ${FILENAME}.key.pem
else	
	if [ "$4" = "rsa" ]
	then
		echo "Using rsa key"
		openssl genrsa -out ${FILENAME}.key.pem 2048
	else
		echo "Invalid criptographic key. Use rsa"
		exit 1
	fi
fi
	
openssl req \
	-config tmp.openssl.user.cnf \
	-key ${FILENAME}.key.pem \
	-new \
	-sha256 \
	-passin pass:tmntpass \
	-subj "/emailAddress=$EMAIL/C=BR/ST=Pr/O=IoT Serpro/CN=${CN}" \
	-out ${FILENAME}.csr.pem
#cd $ROOTDIR/$INTERMEDIATE

openssl ca \
	-config tmp.openssl.user.cnf \
	-passin pass:interpass \
	-days 7500 \
	-md sha256 \
	-in ${FILENAME}.csr.pem \
	-out ${FILENAME}.cert.pem \
	-extensions client_cert

rm ${FILENAME}.csr.pem tmp.openssl.user.cnf
echo $(( $(cat $INTERMEDIATE/serial) + 1 )) > serial
