# Create Public Key Infrastructure

This scripts is used to make a complete PKI from ROOT to users ou server certificates.

## How to use it


### Create Root infrastructure

You can ignore interactive process creating a file batch.txt into base directory

First execute create_root ROOT_PATH
ex: ./create_root.sh /home/user/CAs/rootCA

### Create First Intermediate CA

./intermediate.sh intermatiate_CN  INTERMEDIATE_DIR ROOT_CA_PATH
ex: ./intermediate.sh intemediate1 /home/user/CAs/intemediateCA /home/user/CAs/rootCA


### Create Other Intermediates
 
./home.sh intermatiate_CN CA_home Intermediate_CA_Home
ex: ./home.sh intemediate2 /home/user/CAs/intemediateCA2 /home/user/CAs/intermediateCA


### Create Server Certificate

./server.sh CN Intermediate_CA_Home "alternative_name1 ... alternative_nameN"



## Commands to verify 

openssl req -text -noout -verify -in C
