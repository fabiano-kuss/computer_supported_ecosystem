#!/bin/bash

#Aulacast Certificate Generate
#Copyright (C) 2019  Fabiano Sardenberg Kuss

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ "$3" == "" ]
then
    echo use ./server ip_or_dns intermediate "alternative_name1 ... alternative_nameN"
    exit 0
fi

SN=$1
INTERMEDIATEDIR=$2
EMAIL=$SN@$SN.com

#SET ALTENATIVE NAMES
all_alt_names=$3


for i in $all_alt_names
do
    if [[ $i =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        alt_names=$alt_names"IP:$i,"
    else
        alt_names=$alt_names"DNS:$i,"
    fi
done

if [ "$alt_names" != "" ]
then
    #alt_names="subjectAltName = ${alt_names%?}"
    alt_names="subjectAltName = ${alt_names}DNS:localhost"
fi

cp $INTERMEDIATEDIR/openssl.intermediate.cnf tmp.openssl.server.cnf

echo $alt_names >> tmp.openssl.server.cnf
echo "1.2.3.4.1000=ASN1:UTF8String:teste" >> tmp.openssl.server.cnf

#openssl ecparam -genkey -name secp384r1 -out $SN.key.pem

if [ -z "$4" ]
then
    echo "Using ECC key"
    openssl ecparam -genkey -name secp384r1 -out $SN.key.pem
else
    if [ "$4" = "rsa" ]
    then
        echo "Using rsa key"
        openssl genrsa -out $SN.key.pem 2048
    else
        echo "Invalid criptographic key. Use rsa"
        exit 1
    fi
fi



openssl req \
	-config tmp.openssl.server.cnf \
	-key $SN.key.pem \
	-new \
	-sha256 \
	-passin pass:serverpass \
	-extensions server_cert \
    -subj "/emailAddress=$EMAIL/C=BR/ST=Pr/O=IoT Serpro/CN=$SN" \
	-out $SN.csr.pem 

openssl ca \
	-config tmp.openssl.server.cnf \
	-passin pass:interpass \
	-extensions server_cert \
	-days 7500 \
	-md sha256 \
	-in $SN.csr.pem \
	-out $SN.cert.pem

rm tmp.openssl.server.cnf $SN.csr.pem

echo $(( $(cat $INTERMEDIATEDIR/serial) + 1 )) > serial
