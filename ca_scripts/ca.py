import OpenSSL, sys

"""
Aulacast SBC Module CA Process
Copyright (C) 2019  Fabiano Sardenberg Kuss

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

certfilename = "/tmp/opt1/ca/tmnt/intermediate/certs/intermediate.cert.pem"
certfilekey = "/tmp/opt1/ca/tmnt/intermediate/private/intermediate.key.pem"
reqfile = sys.argv[1]

with open(reqfile, "r") as my_cert_file:
    my_cert_req = my_cert_file.read()
    req = OpenSSL.crypto.load_certificate_request(OpenSSL.crypto.FILETYPE_PEM, my_cert_req)

ext = req.get_extensions()
for i in range(len(ext)):
    k = ext[i]
    print k.get_data()
student = ext[0]
school = ext[1]

print student.get_data()

with open(certfilename, "r") as my_cert_file:
    my_cert_text = my_cert_file.read()
    ca_cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, my_cert_text)

with open(certfilekey, "r") as my_cert_file:
    my_cert_key_text = my_cert_file.read()
    ca_key = OpenSSL.crypto.load_privatekey(OpenSSL.crypto.FILETYPE_PEM, my_cert_key_text)

cert = OpenSSL.crypto.X509()
cert.set_subject(req.get_subject())
#cert.set_version(3)
cert.set_serial_number(1)
cert.gmtime_adj_notBefore(0)
cert.gmtime_adj_notAfter(24 * 60 * 60 * 365)
cert.set_issuer(ca_cert.get_subject())
cert.set_pubkey(req.get_pubkey())
cert.add_extensions([
    OpenSSL.crypto.X509Extension(b'keyUsage', True, b'nonRepudiation, digitalSignature, keyEncipherment'),
    OpenSSL.crypto.X509Extension(b'basicConstraints', False, b'CA:FALSE, pathlen:0'),
    student,
    school
    ])

cert.sign(ca_key, "sha256")

result = OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)

print result

cert = open(reqfile.replace(".csr", ".crt"), "w+")

cert.write(result)
