#!/bin/bash

#Aulacast Certificate Generate
#Copyright (C) 2019  Fabiano Sardenberg Kuss

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

SN=$1
ROOTDIR=$2

replaced_dir=$(echo $ROOTDIR | sed 's/\//\\\//g')


#ls /root 2> /dev/null
#if [ $? != 0 ]
#then
#    echo execute program as root
#    exit 0
#fi

ISBATCH=N
if [ -f batch.txt ]; then ISBATCH=Y; fi

echo Creating directory tree in $ROOTDIR
if [ ! -d $ROOTDIR ] 
then
    if [ $ISBATCH == "N" ]
    then
        read -p "Step 1 - Press Y to make the $ROOTDIR directory tree, N to ignore step: " OPT
    else
        OPT=y
    fi

    typeset -l OPT

    if [ "$OPT" == "y" ] || [ "$OPT" == "Y" ]
    then

        mkdir -p $ROOTDIR/{certs,newcerts,private}
        #mkdir -p $ROOTDIR/intermediate/{certs,csr,newcerts,private}

        chown -R $(whoami) $ROOTDIR
    else
        exit 0
        
    fi
else
    if [ -f $ROOTDIR/certs/root.cert.pem ]
    then 
        echo "You can't rewrite ROOT certificate. This is in $ROOTDIR/certs/root.cert.pem"
        exit 1
    fi
    
    OPT=y
fi

if [ "$OPT" == "y" ] || [ "$OPT" == "Y" ]
then

    EMAIL=$(echo $SN | sed -e  's/ /_/g' | tr '[:upper:]' '[:lower:]')
    EMAIL="$EMAIL@$EMAIL"

	cp openssl.root.cnf $ROOTDIR/openssl.root.cnf

	sed -i "s/-ROOTDIR-/$replaced_dir/g" $ROOTDIR/openssl.root.cnf
	sed -i "s/-COMMONNAME-/$SN/g" $ROOTDIR/openssl.root.cnf
	sed -i "s/-EMAIL-/$EMAIL/g" $ROOTDIR/openssl.root.cnf
    

	cd $ROOTDIR
	touch index.txt
	echo "unique_subject = yes" > index.txt.attr
	echo FFFFFF > serial

	cd $ROOTDIR
	openssl req -config openssl.root.cnf \
		    -x509 \
		    -passout pass:rootpass \
		    -days 7300 \
		    -newkey rsa \
		    -keyout private/root.key.pem \
		    -out    certs/root.cert.pem

	#echo "Inspecting root.cert.pem"

	cd $ROOTDIR
	openssl x509 -noout -text \
	       -in certs/root.cert.pem \
	       -fingerprint -sha256

	#cat $ROOTDIR/intermediate/openssl.intermediate.cnf
fi


echo "ROOT Certificate in $ROOTDIR/certs ROOT key in $ROOTDIR/private."
echo "execute intermediate.sh script: ./intermediate.sh intermediate_name "
echo "execute server.sh script: ./server intermediate \"<server_name_or_ip>\"" "alias0 alias1 aliasip ..."
echo "execute user.sh script: ./server intermediate \"<user complete name>\""
