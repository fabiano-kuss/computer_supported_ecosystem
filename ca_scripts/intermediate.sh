#!/bin/bash

#Aulacast Create intermediate certificate
#Copyright (C) 2019  Fabiano Sardenberg Kuss

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

if [ "$1" == "" ]
then
    echo use ./intermediate intermediate_CN intermediate_dir root_dir
    exit 0
fi

INTERMEDIATE=$1
INTERMEDIATENAME=$INTERMEDIATE
INTERMEDIATEDIR=$2
ROOTDIR=$3

SN=$INTERMEDIATE


ISBATCH=N
if [ -f batch.txt ]; then ISBATCH=Y; fi


replaced_dir=$(echo $INTERMEDIATEDIR | sed 's/\//\\\//g')


if [ ! -d $INTERMEDIATEDIR ]
then
    if [ $ISBATCH == "N" ] 
    then
        read -p "Step 1 - Press Y to make the $INTERMEDIATEDIR directory tree, N to ignore step: " OPT
    else
        OPT=y 
    fi

    typeset -l OPT

    if [ "$OPT" == "y" ] || [ "$OPT" == "Y" ]
    then

        mkdir -p $INTERMEDIATEDIR/{certs,csr,newcerts,private}

        chown -R $(whoami) $INTERMEDIATEDIR
    else
        exit 0
        
    fi
else
    if [ -f $INTERMEDIATE.cert.pem ]
    then
        echo "You can't rewrite $INTERMEDIATE certificate. This is in $INTERMEDIATEDIR/certs/root.cert.pem"
        exit 1
    fi
     
    OPT=y
fi


mkdir -p $INTERMEDIATEDIR
mkdir -p $INTERMEDIATEDIR/{certs,csr,newcerts,private}

cp openssl.intermediate.cnf $INTERMEDIATEDIR/openssl.intermediate.cnf
sed -i "s/-ROOTDIR-/$replaced_dir/g" $INTERMEDIATEDIR/openssl.intermediate.cnf
sed -i "s/-intermediate-/$INTERMEDIATE/g" $INTERMEDIATEDIR/openssl.intermediate.cnf
sed -i "s/-intermediatename-/$INTERMEDIATENAME/g" $INTERMEDIATEDIR/openssl.intermediate.cnf
sed -i "s/-SUBJECTALTNAME-//g" $INTERMEDIATEDIR/openssl.intermediate.cnf

EMAIL=$(echo $SN | sed -e  's/ /_/g' | tr '[:upper:]' '[:lower:]')
EMAIL="$INTERMEDIATENAME@$EMAIL"

sed -i "s/-SUBJECTALTNAME-//g" $INTERMEDIATEDIR/openssl.intermediate.cnf
sed -i "s/-ORGANIZATION-/$SN/g" $INTERMEDIATEDIR/openssl.intermediate.cnf
sed -i "s/--/$SN/g" $INTERMEDIATEDIR/openssl.intermediate.cnf

cd $INTERMEDIATEDIR

touch index.txt
echo  "000001" > serial

cd $INTERMEDIATEDIR/
openssl genrsa \
        -aes256 \
        -passout pass:interpass \
        -out private/$INTERMEDIATENAME.key.pem 2048

cd $INTERMEDIATEDIR/
openssl req \
        -config openssl.intermediate.cnf \
        -new \
        -sha256 \
        -key private/$INTERMEDIATENAME.key.pem \
        -passin pass:interpass \
        -subj "/emailAddress=${EMAIL}.local/C=BR/ST=Pr/O=$SN/CN=$INTERMEDIATENAME" \
        -out csr/$INTERMEDIATENAME.csr.pem

echo $ROOTDIR
openssl ca -batch -config $ROOTDIR/openssl.root.cnf \
           -extensions v3_intermediate_ca \
           -notext \
           -passin pass:rootpass \
           -in $INTERMEDIATEDIR/csr/$INTERMEDIATENAME.csr.pem \
           -out $INTERMEDIATEDIR/certs/$INTERMEDIATENAME.cert.pem
