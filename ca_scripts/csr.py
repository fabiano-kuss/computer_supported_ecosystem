"""
Aulacast SBC Request Certificate
Copyright (C) 2019  Fabiano Sardenberg Kuss

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
import sys

if len(sys.argv) < 2:
    print "Informe o tipo [t/s]"
    sys.exit()

private_key = ec.generate_private_key(ec.SECP384R1(), default_backend())

pubKey = private_key.public_key()

strPrivKey = private_key.private_bytes(encoding=serialization.Encoding.PEM, 
        format=serialization.PrivateFormat.PKCS8, encryption_algorithm=serialization.NoEncryption())

#print pubKey.public_bytes(serialization.Encoding.PEM, serialization.PublicFormat.SubjectPublicKeyInfo)

student_oid = x509.ObjectIdentifier('2.16.76.1.10.1')
school_oid = x509.ObjectIdentifier('2.16.76.1.10.2')
teacher_oid = x509.ObjectIdentifier('2.16.76.1.4.3.1')

attribute_type = sys.argv[1]

nascimento="11121971"
cpf='00000000000'
matricula='000000000000000'
rg='000000000000000'
orgao_uf='SSP     PR'

instituicao='NOME INSTITUICAO                       '
grau_escolaridade='               '
curso='                            '
municipio = '                    '
uf='UF'

csr_builder = x509.CertificateSigningRequestBuilder()
csr_builder = csr_builder.subject_name(
    x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"BR"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Parana"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Curitiba"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"Universidade Federal do Parana (UFPR)"),
        x509.NameAttribute(NameOID.COMMON_NAME, u"Fabiano Sardenberg Kuss")
    ])
)

if attribute_type.lower() == "t":
    csr_builder = csr_builder.add_extension(
            x509.UnrecognizedExtension(teacher_oid, nascimento+cpf+matricula+rg+orgao_uf),
            critical=False
    )
    f = open("t_cert.csr", "w+")
    fk = open("t_cert.key.crt", "w+")
else:
    csr_builder = csr_builder.add_extension(
            x509.UnrecognizedExtension(student_oid, nascimento+cpf+matricula+rg+orgao_uf),
            critical=False
    )
    f = open("s_cert.csr", "w+")
    fk = open("s_cert.key.crt", "w+")

csr_builder = csr_builder.add_extension(
        x509.UnrecognizedExtension(school_oid, instituicao+grau_escolaridade+curso+municipio+uf),
        critical=False
)
csr = csr_builder.sign(private_key, hashes.SHA256(), default_backend())

print csr.public_bytes(serialization.Encoding.PEM)

fk.write(strPrivKey)
fk.close()


f.write(csr.public_bytes(serialization.Encoding.PEM))
f.close()


