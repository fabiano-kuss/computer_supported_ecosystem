/*
 * Mosquitto SSL auth
 * Copyright (C) 2019  Fabiano Sardenberg Kuss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * sudo apt-get install xsltproc
 * sudo apt-get install libssl-dev
 * git clone https://github.com/eclipse/mosquitto.git
 * sudo apt-get install libmosquitto-dev
 * make WITH_DOCS=no
 * sudo cp src/mosquitto /usr/sbin/mosquitto
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <openssl/evp.h>
#include <mosquitto.h>
#include <mosquitto_plugin.h>
#include <fnmatch.h>
#include <time.h>
#include <openssl/x509.h>


#include "log.h"
#include "auth-plug.h"

void *mosquitto_client_certificate(const struct mosquitto *client);

int mosquitto_auth_plugin_version(void)
{
	log_init();
	_log(MOSQ_LOG_NOTICE, "*** Starting auth-plug: startup, mosquitto lib version: %d", MOSQ_AUTH_PLUGIN_VERSION);

	return MOSQ_AUTH_PLUGIN_VERSION;
}

#if MOSQ_AUTH_PLUGIN_VERSION >=3
int mosquitto_auth_plugin_init(void **userdata, struct mosquitto_opt *opts, int opt_count)
#else
int mosquitto_auth_plugin_init(void **userdata, struct mosquitto_auth_opt *opts, int opt_count)
#endif
{
	
	log_init();
	_log(LOG_DEBUG,"0 - Initing");

	OpenSSL_add_all_algorithms();

	//Create hashtable cache
	*userdata = (struct config_info *)malloc(sizeof(struct config_info));
	if (*userdata == NULL) {
		perror("allocting userdata");
		return MOSQ_ERR_UNKNOWN;
	}

	memset(*userdata, 0, sizeof(struct config_info));

	struct config_info *cf;

	cf = *userdata;
	cf->id_start = 0;
	cf->id_end = 0;

	_log(MOSQ_LOG_NOTICE, "OPT_COUNT %ld", 2*sizeof(struct config_info));

        int i;
#if MOSQ_AUTH_PLUGIN_VERSION >=3
	struct mosquitto_opt *o;
#else
	struct mosquitto_auth_opt *o;
#endif

	for (i = 0, o = opts; i < opt_count; i++, o++) {
		if(!strcmp("device_queue", o->key)){
			cf->device_queue = o->value;
		}else if(!strcmp("teacher_queue", o->key)){
                        cf->teacher_queue = o->value;
                }else if(!strcmp("student_queue", o->key)){
                        cf->student_queue = o->value;
                }else if(!strcmp("student_oid", o->key)){
                        cf->student_oid = o->value;
                }else if(!strcmp("teacher_oid", o->key)){
                        cf->teacher_oid = o->value;
                }else if(!strcmp("school_oid", o->key)){
                        cf->school_oid = o->value;
                }else if(!strcmp("id_start", o->key)){
                        cf->id_start = atoi(o->value);
                }else if(!strcmp("id_end", o->key)){
                        cf->id_end = atoi(o->value);
                }
	}

	_log(MOSQ_LOG_NOTICE, "Active Configuration:");
	_log(MOSQ_LOG_NOTICE, "- device queue: %s", cf->device_queue);
	_log(MOSQ_LOG_NOTICE, "- teacher queue: %s", cf->teacher_queue);
	_log(MOSQ_LOG_NOTICE, "- student queue: %s", cf->student_queue);
	_log(MOSQ_LOG_NOTICE, "- student oid: %s", cf->student_oid);
	_log(MOSQ_LOG_NOTICE, "- teacher oid: %s", cf->teacher_oid);
	_log(MOSQ_LOG_NOTICE, "- school oid: %s", cf->school_oid);
	_log(MOSQ_LOG_NOTICE, "- id_start: %d", cf->id_start);
	_log(MOSQ_LOG_NOTICE, "- id_end: %d", cf->id_end);

	return MOSQ_ERR_SUCCESS;
}

#if MOSQ_AUTH_PLUGIN_VERSION >=3
int mosquitto_auth_plugin_cleanup(void *user_data, struct mosquitto_opt *opts, int opt_count)
#else
int mosquitto_auth_plugin_cleanup(void *userdata, struct mosquitto_auth_opt *auth_opts, int auth_opt_count)
#endif
{
	_log(LOG_DEBUG,"1 - Clanning");
	
	return MOSQ_ERR_SUCCESS;
}

#if MOSQ_AUTH_PLUGIN_VERSION >=3
int mosquitto_auth_security_init(void *user_data, struct mosquitto_opt *opts, int opt_count, bool reload)
#else
int mosquitto_auth_security_init(void *userdata, struct mosquitto_auth_opt *auth_opts, int auth_opt_count, bool reload)
#endif
{
	_log(LOG_DEBUG, "2 - Secutity Init");
	return MOSQ_ERR_SUCCESS;
}

#if MOSQ_AUTH_PLUGIN_VERSION >=3
int mosquitto_auth_security_cleanup(void *user_data, struct mosquitto_opt *opts, int opt_count, bool reload)
#else
int mosquitto_auth_security_cleanup(void *userdata, struct mosquitto_auth_opt *auth_opts, int auth_opt_count, bool reload)
#endif
{
	_log(LOG_DEBUG, "3 - Secutity Clean");
	return MOSQ_ERR_SUCCESS;
}

#if MOSQ_AUTH_PLUGIN_VERSION >= 4
int mosquitto_auth_unpwd_check(void *userdata, struct mosquitto *client, const char *username, const char *password)
#elif MOSQ_AUTH_PLUGIN_VERSION >=3 && MOSQ_AUTH_PLUGIN_VERSION < 4
int mosquitto_auth_unpwd_check(void *userdata, const struct mosquitto *client, const char *username, const char *password)
#else
int mosquitto_auth_unpwd_check(void *userdata, const char *username, const char *password)
#endif
{
	return MOSQ_ERR_SUCCESS;
}

#if MOSQ_AUTH_PLUGIN_VERSION >= 4
int mosquitto_auth_acl_check(void *userdata, int access, struct mosquitto *client, const struct mosquitto_acl_msg *msg)
#elif MOSQ_AUTH_PLUGIN_VERSION >=3 && MOSQ_AUTH_PLUGIN_VERSION < 4
int mosquitto_auth_acl_check(void *userdata, int access, const struct mosquitto *client, const struct mosquitto_acl_msg *msg)
#else
int mosquitto_auth_acl_check(void *userdata, const char *clientid, const char *username, const char *topic, int access)
#endif
{
struct config_info *cf = (struct config_info *)userdata;

	//clock_t tic = clock();
	
        X509 *cert = mosquitto_client_certificate(client);

	if(cert == NULL){
		printf("NO CERT");
		return MOSQ_ERR_SUCCESS;
	}	
        char *subj = X509_NAME_oneline(X509_get_subject_name(cert), NULL, 0);
        //char *issuer = X509_NAME_oneline(X509_get_issuer_name(cert), NULL, 0);

	int ext_count = X509_get_ext_count(cert);

	int i = 0;
	//int t_id = 0;
	//char *user_queue;
	X509_EXTENSION *ext;
	for(i = 0; i < ext_count; i++){
		ext = X509_get_ext(cert, i);
		ASN1_OBJECT *obj = X509_EXTENSION_get_object(ext);
		char buff[128];
		OBJ_obj2txt(buff, 1024, obj, 0);
		//char *dtvalue = (char *)ext->value->data;

		if(!strcmp(buff, cf->device_queue)){
			if(!strncmp(msg->topic, "sensor/", strlen("sensor/")))
				return MOSQ_ERR_SUCCESS;
				
		}
		/*if(!strcmp(buff, cf->teacher_oid)){
			t_id = 1;
		}else if(!strcmp(buff, cf->student_oid)){
			t_id = 0;
			if(access == MOSQ_ACL_WRITE){
				if(strncmp(cf->student_queue, msg->topic, strlen(cf->student_queue))){
					printf("RETURN ERROR on topic %s: TID: %s %f seconds\n", msg->topic, dtvalue,  (double)(clock() - tic) / CLOCKS_PER_SEC);
					return MOSQ_ERR_ACL_DENIED;
				}

				user_queue = (char *)malloc(strlen(dtvalue));
				if(cf->id_end > 0)
					strncpy(user_queue, dtvalue + cf->id_start, cf->id_end);
				else
					strcpy(user_queue, dtvalue);
				printf("QUEUE NAME: %s\n", user_queue);
			}
		}*/
		free(ext);
	}

	/*
	if(access == MOSQ_ACL_WRITE)
		printf("Write Topic: %s TID: %d Elapsed: %f seconds\n", msg->topic, t_id, (double)(clock() - tic) / CLOCKS_PER_SEC);
	else
		printf("Read Topic: %s TID: %d Elapsed: %f seconds\n", msg->topic, t_id, (double)(clock() - tic) / CLOCKS_PER_SEC);
	*/
        free(subj);

	if(access != MOSQ_ACL_WRITE)
		return MOSQ_ERR_SUCCESS;
	return MOSQ_ERR_ACL_DENIED;

}


#if MOSQ_AUTH_PLUGIN_VERSION >= 4
int mosquitto_auth_psk_key_get(void *userdata, struct mosquitto *client, const char *hint, const char *identity, char *key, int max_key_len)
#elif MOSQ_AUTH_PLUGIN_VERSION >=3 && MOSQ_AUTH_PLUGIN_VERSION < 4
int mosquitto_auth_psk_key_get(void *userdata, const struct mosquitto *client, const char *hint, const char *identity, char *key, int max_key_len)
#else
int mosquitto_auth_psk_key_get(void *userdata, const char *hint, const char *identity, char *key, int max_key_len)
#endif
{
	_test("6 Auth pks check");
	return MOSQ_ERR_SUCCESS;
}
