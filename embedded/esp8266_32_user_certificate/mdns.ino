String formatHostName(String hostName){
  long len = hostName.length();
  if(hostName.endsWith("local"))
    len -= 5;
  return hostName.substring(len-6, len-1);
}

#ifdef ESP32
String showMDNSResults(mdns_result_t * results, char **devices, int *dev_size){
  mdns_result_t * r = results;
  
  String ret_val = "";

  int cnt = 0;
  while(r){
    devices[cnt] = (char *)malloc(sizeof(char) * (sizeof(r->hostname) + 1));
    strcpy(devices[cnt], formatHostName(String(r->hostname)+" ").c_str());
    cnt++;
    ret_val += formatHostName(String(r->hostname)+" ")+"|";
    r = r->next;
  }

  *dev_size = cnt;
  return ret_val;

}


String findMDNSResults(const char * service_name, const char * proto, char **devices, int *dev_size){

  mdns_result_t * results = NULL;
  esp_err_t err = mdns_query_ptr(service_name, proto, 2000, 20,  &results);
  if(err){
      ESP_LOGE(TAG, "Query Failed");
      return "FAIL";
  }
  if(!results){
      ESP_LOGW(TAG, "No results found!");
      return "NO_RES";
  }
  
  String ret_val = showMDNSResults(results, devices, dev_size);
  mdns_query_results_free(results);
  //Serial.println("RET VAL: "+ret_val);
  return ret_val;
}

#else
String findMDNSResults(String service_name, String proto, char **devices, int *dev_size){
  int n = MDNS.queryService(service_name, proto);
  String ret_val = "";
  String host_name;
  for (int i = 0; i < n; ++i) {
    long len = MDNS.hostname(i).length();
    //Serial.println(MDNS.hostname(i));
    if(MDNS.hostname(i).endsWith("local"))
      len -= 5;
    host_name = MDNS.hostname(i).substring(len-6, len-1);
    ret_val += host_name+"|";
    devices[i] = (char *)malloc(sizeof(char) * (sizeof(host_name) + 1));
    strcpy(devices[i], formatHostName(String(host_name)+" ").c_str());
  }
  *dev_size = n;
  return ret_val;
}
#endif
