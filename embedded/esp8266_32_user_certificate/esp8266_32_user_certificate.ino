//Define in PubSubClient.h
#define MQTT_KEEPALIVE 120
#define MQTT_MAX_PACKET_SIZE 128
#ifdef ESP32
#include <WiFi.h>
#include <PubSubClient.h>
#include <ESPmDNS.h>
#include <FS.h>
#include "SPIFFS.h"
#include "mdns.h"
#else
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <sys/time.h>
#endif
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
//#include "certs.h"

//openssl x509 -in /tmp/opt1/ca/c3sl/certs/Fabiano_Sardenberg_Kuss.cert.pem -out /tmp/Fabiano_Sardenberg_Kuss.bin.cert  -outform DER
//openssl ec -in /tmp/opt1/ca/c3sl/private/Fabiano_Sardenberg_Kuss.key.pem -out /tmp/Fabiano_Sardenberg_Kuss.bin.key  -outform DER
//xxd -i /tmp/Fabiano_Sardenberg_Kuss.bin.cert > /tmp/certs.h
//xxd -i /tmp/Fabiano_Sardenberg_Kuss.bin.key >> /tmp/certs.h
#ifdef ESP32
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"
#endif

typedef struct conf{
    int version;
    short ssl = 1;
    char broker[64];
    int broker_port;
    char ap[64];
    char ap_pwd[64];
    
}conf;

const char caCert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDszCCApugAwIBAgIEAP///zANBgkqhkiG9w0BAQsFADBpMRUwEwYDVQQDDAxD
M1NMIFJvb3QgQ0ExCzAJBgNVBAgMAlByMQswCQYDVQQGEwJCUjEnMCUGCSqGSIb3
DQEJARYYY2VydGlmaWNhdGVAbXltYWlsLmxvY2FsMQ0wCwYDVQQKDARDM1NMMB4X
DTIwMDYwNjIwMzczNFoXDTQwMDYwMTIwMzczNFowZjEYMBYGA1UEAwwPSW50ZXJt
ZWRpYXRlIENBMQswCQYDVQQIDAJQcjELMAkGA1UEBhMCQlIxITAfBgkqhkiG9w0B
CQEWEmFkbWluQHNjaG9vbC5sb2NhbDENMAsGA1UECgwEQzNTTDCCASIwDQYJKoZI
hvcNAQEBBQADggEPADCCAQoCggEBAKqcOlO8JED1+bIpSU27T5QYOwsMRzTjImuP
dQfcWkDErMDY8Tds6cnFf4T84MEFLHTbh0nGofzyRFyPcN2UTvkgcHPjK3xeCHSU
zj2/fsKRUkQy8hmrJ6vEUaiFigruP4C/ZP9SjK10DfTq+AeBaP8NgAjji3Dn0xsN
62YflSZ4EwGllEVaANdt52Ih7esCMCpm8Z9mFPrLS11xpaHhJijyhDMal3S64QTv
CYKZejyJbAZQzOXutMMsSo7Wj3ikZjISd702o+TpQm4bCM4QFVvwA+/lLXRe+oxa
FThw1FtS81v7Dyx1tld0Gb/s3ZVa+QKLDQn0y8qh22DkrNGh3ccCAwEAAaNmMGQw
HQYDVR0OBBYEFA53i4yF5zJ1rJY1Nxc6RXCxqxa2MB8GA1UdIwQYMBaAFAX15oL1
dmiQGRkL0OCK48jAHk9PMBIGA1UdEwEB/wQIMAYBAf8CAQAwDgYDVR0PAQH/BAQD
AgKEMA0GCSqGSIb3DQEBCwUAA4IBAQBSsIU7uGimye8l2KSotzvj2i/2cfy0Ap05
twv5a+nn1bge2C0aeMf/FpeQez/fMneyFt3zvc9iiQSH/pp7cQE3t6KOkqTvDQMA
X6tvzCGvf7Q0Jv8OTCT/5xPw0+dceWPf2doy0R7tWgMzeAX34VGqjSPr5E/qig+L
vyU+GfX/pbCZbV4Mt49kqQHA1X0dkzXQf7zjV5uRGF5HZomwepFpp8l3Sg3jrC1i
GGLREkOyouIdlbgMKa8IknavP6Bth/fskHIYERwfJNY4DOPDSklt8t8Pkkw0BXU4
GPEUuSJrez4Gf4ADrFNW84oDomDNOz/vZprw0Igeb5NbJwRTo57e
-----END CERTIFICATE-----
)EOF";


conf cfg;
//#define NOSSL
#ifdef NOSSL
WiFiClient espClient;
#else
#ifdef ESP32
WiFiClientSecure espClient;
#else
BearSSL::WiFiClientSecure espClient;
#endif
#endif
PubSubClient mqttClient(espClient); /* MQTT Client connection */
#ifdef ESP32
String clientId = "ESP32_";
#else
String clientId = "ESP8266_";
#endif
String id_number = "";

String certData ="";
String certKey ="";


int MDNS_update = 0;

unsigned long wifi_time = 0;
unsigned long mdns_time = 0;
unsigned long mqtt_time = 0;
unsigned long config_time = 0;
unsigned int attemptations = 0;
unsigned long pub_id = 0;
unsigned int rnd_msg_size[] = {10, 25, 50, 75};
int mktt_reconect = -1;
int mktt_reconect_fail = -1;
int mktt_send_fails = 0;
String start_results =  "";
bool connected = false;
String reset_reason = "";
int count = 0;
unsigned long start_time;
long last_find = -30000;
long last_loop = -1000;


void reconnect() {
  while (!mqttClient.connected()) {
    attemptations++;
    mktt_reconect++;
    
#ifndef NOSSL
#ifdef ESP32
    espClient.setCACert(caCert);
#else
    espClient.setCACert((const uint8_t *)caCert, sizeof(caCert));
    X509List client_crt((const char*)certData.c_str());
    PrivateKey key((const char*)certKey.c_str());
    espClient.setClientECCert(&client_crt, &key, 0xFFFF, 0);
    
#endif
#endif
    //espClient.connect(cfg.broker, cfg.broker_port);
    Serial.print("Attempting MQTT with client "+clientId+" broker connection...");
    if (mqttClient.connect(clientId.c_str())) {
      Serial.println("connected");
      mqttClient.subscribe("sensor/in");
      mqttClient.subscribe("sensor/command"); 
      mqttClient.subscribe("sensor/change");
      String my_topic = "devices/"+id_number;
      mqttClient.subscribe(my_topic.c_str());
      String my_results = "results/"+id_number;
      mqttClient.subscribe(my_results.c_str());
      mktt_reconect++;
    }else {
      mktt_reconect_fail++;
      Serial.print("ESP.getFreeHeap(): ");
      Serial.println(ESP.getFreeHeap());
      Serial.print("Failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(". Trying again in 1 seconds...");
      if(mktt_reconect_fail <= 10)
        delay(1000 * mktt_reconect_fail);
      else if(mktt_reconect_fail > 10)
        ESP.restart();
    }
  }
}

void configEnvironment(){
  
  config_time = millis();
  SPIFFS.begin();
  if(!SPIFFS.exists("/conf.cfg")){
    File c = SPIFFS.open("/conf.cfg", "w");
    cfg.version = 1; 
    strcpy(cfg.broker, "mybroker");
    cfg.broker_port = 28883;
    strcpy(cfg.ap, "myap");
    strcpy(cfg.ap_pwd, "myappwd_or_blank");
    Serial.println("################CONFIG NOT FOUND###################");
    c.write((uint8_t *)&cfg, sizeof(conf));
    c.close();
  }else if(SPIFFS.exists("/conf.cfg")){
    File c = SPIFFS.open("/conf.cfg", "r");
    c.read((uint8_t *)&cfg, sizeof(conf));
    Serial.println(cfg.broker);
    Serial.println(cfg.broker_port);
    Serial.println(cfg.ap);
    Serial.println(cfg.ap_pwd);
    Serial.println(cfg.ssl);
#ifdef NOSSL
    cfg.broker_port = 1883;
#endif
    Serial.println("################CONFIG INFO###################");
    Serial.println(cfg.broker);
    Serial.println(cfg.broker_port);
    Serial.println(cfg.ap);
    Serial.println(cfg.ap_pwd);
    Serial.println(cfg.ssl);
    Serial.println("##############END CONFIG INFO#################");
    c.close();
  }

#ifdef ESP32
  
  File root = SPIFFS.open("/");
  File file = root.openNextFile();
  while(file){
      if(String(file.name()).endsWith(".cert.pem")){
        Serial.println("CERTIFICATE FILE FOUND:" +String(file.name()));
        File f = SPIFFS.open(file.name(), "r");
        while(f.available()){
          certData += char(f.read());
        }
        f.close();
      }else if(String(file.name()).endsWith(".key.pem")){
        Serial.println("KEY FILE FOUND:" +String(file.name()));
        File f = SPIFFS.open(file.name(), "r");
        while(f.available()){
          certKey += char(f.read());
        }
        f.close();
      }
      file = root.openNextFile();
  }
  file.close();
#else
  Dir dir = SPIFFS.openDir("/");
  while (dir.next()) {
    if(dir.fileName().endsWith(".cert.pem")){
      Serial.println("CERTIFICATE FILE FOUND: "+dir.fileName());
      File f = SPIFFS.open(dir.fileName(), "r");
      while(f.available()){
        certData += char(f.read());
      }
      f.close();
      //Serial.println(certData.c_str());
    }else if(dir.fileName().endsWith(".key.pem")){
      Serial.println("KEY FILE FOUND: "+dir.fileName());
      File f = SPIFFS.open(dir.fileName(), "r");
      while(f.available()){
        certKey += char(f.read());
      }
      f.close();
    }
  }
#endif
  config_time = millis() - config_time;
}
void setup() 
{
  
  Serial.begin(115200);
  //Serial.setDebugOutput(true);
#ifdef ESP32
  //esp_log_level_set("*", ESP_LOG_VERBOSE);
#endif
  Serial.println();
#ifndef ESP32  
  reset_reason = ESP.getResetReason();
  Serial.println("RESER REASON: "+reset_reason);
#endif;  
  configEnvironment();
  
  wifi_time = millis();
#ifndef ESP32
  WiFi.mode(WIFI_STA);
#endif
  WiFi.begin(cfg.ap, cfg.ap_pwd);
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
    Serial.print(".");
  }
  wifi_time = millis() - wifi_time;

  id_number = WiFi.macAddress().substring(12, WiFi.macAddress().length());
  id_number.replace(':', '_');
  clientId += id_number;

  time_t now;
  tm* localtm;
  //If you needed time sincronizations
  /*udp_time = millis();
  //configTime(0, 0, "pool.ntp.org");
  configTime(0, 0, "raspberrypizerow.local");
  settimeofday(0, nullptr);
  setenv("TZ", "CET-1CEST,M3.5.0,M10.5.0/3", 3);
  tzset();
  now = time(NULL);
  localtm = localtime(&now);
  udp_time = millis() - udp_time*/;
  //Certificate validation needs data validation
  struct tm tm;
  tm.tm_year = 2020 - 1900;
  tm.tm_mon = 6;
  tm.tm_mday = 07;
  tm.tm_hour = 14;
  tm.tm_min = 10;
  tm.tm_sec = 10;
  time_t t = mktime(&tm);
  printf("Setting time: %s", asctime(&tm));
  struct timeval now1 = { .tv_sec = t };
  settimeofday(&now1, NULL);
  
  t = mktime(&tm);
  now = time(0);
  localtm = localtime(&now);
  printf("The local date and time is: %s", asctime(localtm));
  
  /* When WiFi connection is complete, debug log connection info */
  Serial.println();
  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());

#ifndef NOSSL

#ifdef ESP32
  
  if(cfg.ssl){
    espClient.setCACert(caCert);
    espClient.setCertificate(certData.c_str());
    espClient.setPrivateKey(certKey.c_str());
    //espClient.setClientECCert(&client_crt, &key, 0xFFFF, 0);
  }
#else
  if(cfg.ssl){
    espClient.setCACert((const uint8_t *)caCert, sizeof(caCert));
    X509List client_crt((const char*)certData.c_str());
    PrivateKey key((const char*)certKey.c_str());
    espClient.setClientECCert(&client_crt, &key, 0xFFFF, 0);
  }
#endif
#endif
  /* Configure MQTT Broker settings */
  mqttClient.setServer(cfg.broker, cfg.broker_port);
  mqttClient.setCallback(subCallback);

  /* Configure Zeroconfig Broker settings */
  mdns_time = millis();
#ifdef ESP32
  if(!MDNS.begin(clientId.c_str())) {
#else
  if(!MDNS.begin(clientId)) {
#endif
    Serial.println("Error setting up MDNS responder!");
  }else{
    Serial.println("MDNS Started on service with name: "+clientId);
  }
  
  MDNS.addService("termometer", "tcp", 8002);
  MDNS.addServiceTxt("termometer", "tcp", "temperature", "8003");
  MDNS.addServiceTxt("termometer", "tcp", "humidity", "8004");
  //MDNS.update();
  mdns_time = millis() - mdns_time;

}


void loop() {
  
  if(!mqttClient.connected()) {
    reconnect();
  }else{
    if(wifi_time > 0){
      Serial.println("####TIMES#####");
      int total = millis();
      long rssi = WiFi.RSSI();
      start_results= "{\"cli\":\""+clientId+"\",\"wifi\":"+String(wifi_time)+", \"mdns\":"+String(mdns_time)+", \"rssi\":"+String(rssi)+
        ", \"att\":"+String(attemptations)+", \"conf\":"+String(config_time)+", \"tot\":"+String(total)+"}";
      Serial.println(start_results);
      Serial.println("####RESULT PUB#####");
      Serial.println(String(mqttClient.publish("sensor/start", start_results.c_str())));
      mqttClient.publish("sensor/start", (clientId+":"+reset_reason).c_str());
      wifi_time = 0;
    }
    if(millis() - last_loop > 1000){
      mqttClient.loop();
      last_loop = millis();
    }
    if(millis() - last_find > 10000){
      char *str;
      char find_result[128];
      #ifndef ESP32
      char *devices[10];
      #else
      char **devices = (char **)malloc(sizeof(char**) * 10);
      #endif
      int dev_size = 0;
      int send_status = -1;
      
      strcpy(find_result, findMDNSResults("_termometer", "_tcp", devices, &dev_size).c_str());

      //Serial.println("SIZE : " + String(dev_size));
      if(dev_size > 0){
        int rnd = random(dev_size);
        String target = "devices/"+String(devices[rnd]);
        //Serial.println(target);
        char pub_target[128];
        int pub_msg_size = rnd_msg_size[random(sizeof(rnd_msg_size)/sizeof(int))];
        //Serial.println(sizeof(rnd_msg_size)/sizeof(int));
        String m_format = "%s:%010lu:%s:%0"+String(pub_msg_size)+"d:%d";
        #ifdef ESP332
        sprintf(pub_target, m_format.c_str(),id_number, millis(), devices[rnd], 0, pub_msg_size);
        #else
        sprintf(pub_target, m_format.c_str(), id_number.c_str(), millis(), devices[rnd], 0, pub_msg_size);
        #endif
        //Serial.println("TARGET: "+target+" "+pub_target);
        send_status = mqttClient.publish(target.c_str(), pub_target);
      }
      #ifndef ESP32
      for(int i = 0; i < dev_size; i++){
        free(devices[i]);
      }
      #else
      free(devices);
      #endif
      
        
      char *p = find_result;
      pub_id++;
      long rssi = WiFi.RSSI();
      long pub_time = millis();
      
      char pub[128];
      sprintf(pub, "%ld;%s;%s;%i;%i;%ld;%i;%i;%i;", pub_id, clientId.c_str(), find_result, mktt_send_fails, mktt_reconect, pub_time, rssi, send_status,ESP.getFreeHeap(), mktt_reconect_fail);
      Serial.print(pub);
      //Serial.print(pub);
      if(!mqttClient.publish("sensor/status", pub)){
        Serial.println("***********************FAIL");
        mktt_send_fails++;
      }else{
        Serial.println("********************SUCCESS");
        mktt_send_fails = 0;
        mktt_reconect = 0;
        mktt_reconect_fail = 0;
      }
        
      last_find = millis();
      
    }

  }
  
  
#ifndef ESP32
  if(MDNS_update++ > 1000){
    MDNS.update();
    MDNS_update = 0;
  }
#endif
  
  
  if (Serial.available() > 0) {
    String incomingString = Serial.readString();
    Serial.print("I sended: ");
    Serial.println(incomingString);

    if(!mqttClient.publish("sensor/out", incomingString.c_str())){
      Serial.println(F("Publish Failed."));
    }else {
      Serial.println(F("Publish Success!"));
    }

  }
}

void subCallback(char *topic, byte *payload, unsigned int len)
{
  String s = String((char*)payload);
  String t = String(topic);
  long rssi = 0;

  t = t.substring(0);
  s = s.substring(0, len);
  
  //Serial.println("COMMAND: ");
  if(t == "sensor/command"){
    if(s == "restart"){
      Serial.println("RESTARTING");
      delay(500);
      ESP.restart();
    }
  
  }else if(t == "devices/"+id_number){ //New request recivied
    //Serial.println("NEW MESSAGE: "+s);
    if(len > 21){
      String device = s.substring(0,5);
      String message_id = s.substring(6,16);
      String rec_size = s.substring(s.length()-2, s.length());
      int result = mqttClient.publish(("results/"+device).c_str(), (device+":"+message_id+":"+String(id_number)+":"+rec_size).c_str());
      //Serial.println("DEVICE: "+device+" MID: "+message_id+" RESULT: "+String(result));
    }
  }else if(t == "results/"+id_number){ //New response recivied
    //Serial.println("NEW RESULT : "+s);
    int result = mqttClient.publish(("logs/"+id_number).c_str(), (s+":"+t+":"+String(millis())).c_str());
    Serial.println(s+":"+t+":"+String(millis())+":"+String(result));
    
  }else if(t == "sensor/change"){
    File c = SPIFFS.open("/port.cfg", "w");
    int port = s.toInt();
    Serial.print("################CHANGING PORT FILE STATUS");
    Serial.println(c.write((uint8_t *)&port, sizeof(int)));
    c.close();   
  }
}
