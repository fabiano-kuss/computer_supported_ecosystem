import paho.mqtt.client as mqtt
import sys, threading, time

waiting = False
conn_type = ""
def on_connect(client, userdata, flags, rc):
   global waiting, size, steps, qtd
   client.subscribe("sensor/out")

   try:
      steps = int(sys.argv[1])
      qtd = int(sys.argv[2])
      size = int(sys.argv[3])
   except:
      print("Invalid args. Use steps_number msgs_by_step message_size. Optional use no as no tls")
      sys.exit(0)


   t = threading.Thread(target=execute, args=(steps, qtd, size)).start()
   t.join()
   waiting = False
   #execute(steps, qtd, size)

def on_message(client, userdata, msg):
   global waiting
   if len(sys.argv) > 4:
      print (msg.payload.decode()+";"+str(size)+";"+str(steps)+";"+str(qtd)+";no;no")
   else:
      print (msg.payload.decode()+";"+str(size)+";"+str(steps)+";"+str(qtd)+";yes;no")
   waiting = False

def make_message(size):
   msg = ""
   for i in range(0, size):
      msg += "."
   return msg

def execute(steps, qtd, size):
   global waiting, client

   #print("Starting with", steps,  "steps number ", qtd, "msgs by step", size,  "as message size")


   #print(waiting, steps)
   for i in range(0, steps):
      while(waiting):
         time.sleep(1)
      waiting = True
      publish(int(qtd), make_message(size))
   while(waiting):
      time.sleep(1)
   client.disconnect()
   

def publish(size, msg):
     client.publish("sensor/in", "start", 0, False)

     for i in range(0, size):
        client.publish("sensor/in", msg, 0, False)

     client.publish("sensor/in", "stop", 0, False)


client = mqtt.Client()

client.on_connect = on_connect
client.message_callback_add("sensor/out", on_message)
print(sys.argv)
if len(sys.argv) > 4:
   if sys.argv[4] == "no":
      client.connect("192.168.0.158",1883,60)
   else:
      print("INICIANDO COM CERTIFICADO")
      client.tls_set(ca_certs="/etc/mosquitto/ca_certificates/c3sl.cert.pem", certfile=sys.argv[4], keyfile=sys.argv[5])
      print("CERTIFICADO CARREGADO")
      client.connect("superhomer.local", 28883,60)
else:
   print("INICIANDO COM CERTIFICADO DA CA")
   client.tls_set(ca_certs="/etc/mosquitto/ca_certificates/c3sl.cert.pem")
   client.connect("superhomer.local", 8883,60)

try:
   client.loop_forever()
   sys.exit(0)
except KeyboardInterrupt:
   #print("\nCtrl+C pressionado...")
   sys.exit(0)

