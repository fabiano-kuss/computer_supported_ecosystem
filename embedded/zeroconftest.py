from zeroconf import ServiceBrowser, Zeroconf
from time import time


class MyListener:

    def remove_service(self, zeroconf, type, name):
        print("###Service %s removed" % (name,))

    def add_service(self, zeroconf, type, name):
        x = time()
        print(x)
        info = zeroconf.get_service_info(type, name)
        print("###Service %s added, service info: %s" % (name, info))


zeroconf = Zeroconf()
listener = MyListener()
browser = ServiceBrowser(zeroconf, "_termometer._tcp.local.", listener)
try:
    input("Press enter to exit...\n\n")
finally:
    zeroconf.close()
