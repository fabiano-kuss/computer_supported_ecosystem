import paho.mqtt.client as mqtt
import sys, threading, time, os

conn_type = ""
cnt = 0
cnt1 = 0
cnt2 = 0
cnt3 = 0

if not os.path.exists("./logs"):
   os.makedirs("./logs")
log_message = open("logs/messages.log", "w")
log_start = open("logs/start.log", "w")
log_device = open("logs/devices.log", "w")
log_result = open("logs/result.log", "w")
log_log = open("logs/log.log", "w")

def on_connect(client, userdata, flags, rc):
   client.subscribe("devices/#")
   client.subscribe("results/#")
   client.subscribe("logs/#")
   client.subscribe("sensor/status")
   client.subscribe("sensor/start")

def on_message(client, userdata, msg):
   global cnt
   #print("ON MESSAGE: "+msg.payload.decode())
   log_message.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   if cnt == 20:
      log_message.flush()
      cnt = 0
   cnt += 1

def on_start(client, userdata, msg):
   log_start.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   log_start.flush()

def on_device(client, userdata, msg):
   global cnt1
   print("ON DEVICE: "+msg.payload.decode())
   log_device.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   if cnt1 == 10:
      log_start.flush()
      cnt1 = 0;
   cnt1 += 1

def on_result(client, userdata, msg):
   global cnt2
   print("ON RESULT: "+msg.payload.decode())
   log_result.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   if cnt2 == 10:
      log_start.flush()
      cnt2 = 0
   cnt2 += 1

def on_log(client, userdata, msg):
   global cnt3
   print("ON LOG: "+msg.payload.decode()+" CNT: "+str(cnt))
   log_log.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   if cnt3 == 10:
      log_start.flush()
      cnt3 = 0
   cnt3 += 1; 

client = mqtt.Client()

client.on_connect = on_connect
client.message_callback_add("sensor/status", on_message)
client.message_callback_add("sensor/start", on_start)
client.message_callback_add("devices/#", on_device)
client.message_callback_add("results/#", on_result)
client.message_callback_add("logs/#", on_log)

if len(sys.argv) < 5:
   print("Invalid parameters\n Use broker_addr broker_port ca_cert certfile keyfile")
   sys.exit(1)

#print(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5]);

#sys.exit(0);

client.tls_set(ca_certs=sys.argv[3], certfile=sys.argv[4], keyfile=sys.argv[5])
client.connect(sys.argv[1], int(sys.argv[2]), 60)

try:
   client.loop_forever()
   sys.exit(0)
except KeyboardInterrupt:
   #print("\nCtrl+C pressionado...")
   sys.exit(0)

