#ifdef ESP32
#include <FS.h>
#include "SPIFFS.h"
#else
#include <ESP8266WiFi.h>
#endif

void setup() {
  Serial.begin(115200);
  delay(500);
  Serial.println();
  
  SPIFFS.begin();
#ifdef ESP32
  File root = SPIFFS.open("/");
  File file = root.openNextFile();
  while(file){
      if(String(file.name()).endsWith(".cert.pem") || String(file.name()).endsWith(".key.pem")){
        SPIFFS.remove(file.name());
      }
      file = root.openNextFile();
  }
#else
  Dir dir = SPIFFS.openDir("/");
  while (dir.next()) {
    if(dir.fileName().endsWith(".cert.pem") | dir.fileName().endsWith(".key.pem")){
      SPIFFS.remove(dir.fileName());
    }
  }
#endif

}

void loop() {
  

#ifdef ESP32
  File root = SPIFFS.open("/");
  File file = root.openNextFile();
  while(file){
      Serial.println(String(file.name()));
      file = root.openNextFile();
  }
#else
  Dir dir = SPIFFS.openDir("/");
  while (dir.next()) {
      Serial.println(dir.fileName());
  }
#endif


  
  delay(5000);

}
