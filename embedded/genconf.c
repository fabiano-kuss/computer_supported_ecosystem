#include <stdio.h>
#include <string.h>

typedef struct conf{
    int version;
    short ssl;
    char broker[64];
    int broker_port;
    char ap[64];
    char ap_pwd[64];
    
}conf;

int main(int argc, char **argv){
	FILE *f;
	if(argc < 5){
		printf("Pass the values of use_ssl broker broker_port ap ap_pwd\n");
		return -1;
	}
	printf("USE SSL %s Broker: %s Port %s AP: %s AP PWD %s\n", argv[1], argv[2], argv[3], argv[4], argv[5]);
	if(argv[5] == NULL)
		printf("No ap password\n");
	conf cfg;
	cfg.version = 1; 
	cfg.ssl = atoi(argv[1]);
    	strcpy(cfg.broker, argv[2]);
	cfg.broker_port = atoi(argv[3]);
	strcpy(cfg.ap, argv[4]);
	if(argv[5] == NULL)
		strcpy(cfg.ap_pwd, "");
	else
		strcpy(cfg.ap_pwd, argv[5]);
	f = fopen("conf.cfg", "wb+");
	fwrite(&cfg, sizeof(cfg), 1, f);
}
