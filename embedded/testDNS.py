import paho.mqtt.client as mqtt
import sys, threading, time

waiting = False
conn_type = ""
cnt = 0
log = open(sys.argv[1], "w")
log_start = open(sys.argv[1]+".start", "w")

def on_connect(client, userdata, flags, rc):
   client.subscribe("sensor/status")
   client.subscribe("sensor/start")
   client.subscribe("devices/#")
   client.subscribe("results/#")
   client.subscribe("logs/#")

def on_message(client, userdata, msg):
   global cnt
   log.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   if cnt == 20:
      log.flush()
      cn = 0
   cnt += 1
   waiting = False

def on_start(client, userdata, msg):
   log_start.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   log_start.flush()

def on_device(client, userdata, msg):
   log_start.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   log_start.flush()

def on_result(client, userdata, msg):
   log_start.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   log_start.flush()

def on_log(client, userdata, msg):
   log_start.write(msg.payload.decode()+";"+str(int(time.time()*1000))+"\n")
   log_start.flush()


client = mqtt.Client()

client.on_connect = on_connect
client.message_callback_add("sensor/status", on_message)
client.message_callback_add("sensor/start", on_start)
client.message_callback_add("devices/#", on_device)
client.message_callback_add("results/#", on_result)
client.message_callback_add("logs/#", on_log)

if len(sys.argv) > 2:
   if sys.argv[1] == "no":
      print("INICIANDO COM CERTIFICADO DA CA")
      client.tls_set(ca_certs="/etc/mosquitto/ca_certificates/c3sl.cert.pem")
      client.connect("superhomer.local", 8883,60)
   else:
      print("INICIANDO COM CERTIFICADO")
      client.tls_set(ca_certs="/etc/mosquitto/ca_certificates/c3sl.cert.pem", certfile=sys.argv[4], keyfile=sys.argv[5])
      print("CERTIFICADO CARREGADO")
      client.connect("superhomer.local", 28883,60)
else:
   client.connect("raspberrypizerow",1883,60)
try:
   client.loop_forever()
   sys.exit(0)
except KeyboardInterrupt:
   #print("\nCtrl+C pressionado...")
   sys.exit(0)

